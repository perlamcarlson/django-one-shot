from ast import Delete
from unicodedata import name
from django import forms
from django.db import models


class TodoList(models.Model):
    name = models.CharField(max_length=100)
    created_on= models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return(self.name)

class TodoItem(models.Model):
    task= models.CharField(max_length=100)
    due_date= models.DateTimeField(auto_now_add=True)
    is_complete= (True)
    list = models. ForeignKey(TodoList, related_name ="items",
    on_delete= models.CASCADE, null=True, )
    def __str__(self):
        return(self.name)

     
